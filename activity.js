function addNumbers(a, b) {
  console.log(a + b);
}
// const addNumbers = (a, b) => console.log(a + b);
addNumbers(4, 3);

function subtractNumbers(a, b) {
  console.log(a - b);
}
// const subtractNumbers = (a, b) => console.log(a - b);
subtractNumbers(5, 3);

function multiplyNumbers(a, b) {
  return a * b;
}
// const multiplyNumbers = (a, b) => a * b;

const product = multiplyNumbers(2, 3);
console.log(product);
